clear all;
close all;
load common.mat directory targetDir distortionFileSet...
    cuttingDir cuttedFileSet utilsDir
load([targetDir distortionFileSet]);
addpath(utilsDir); addpath(cuttingDir);
cuttedset = fileset;
filePrefix = 'cutted_';
%Test
fileCount = length(fileset);
fprintf('Image to processing: %d\n', fileCount);
wb = waitbar(0, 'Cutting images...');
try
    for i=1:fileCount
        item = fileset(i);
        corrupted = imread(fileset(i).file);
        [cutted, wasNotProcessed] = cutting(corrupted);
        filename = [targetDir filePrefix int2str(i) '.png'];
        cuttedset(i).file = filename;
        cuttedset(i).wasNotProcessed = wasNotProcessed;
        imwrite(cutted, filename);
        waitbar(i / fileCount, wb);
    end
    save([targetDir cuttedFileSet], 'cuttedset');
    close(wb);
catch ME
    close(wb);
    rethrow(ME);
end