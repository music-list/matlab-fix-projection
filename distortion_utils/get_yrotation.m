function [rotation_matrix] = get_yrotation(y_deg)
rotation_matrix = [
    cosd(y_deg)  0 -sind(y_deg);...
    0            1 0           ;...
    sind(y_deg)  0 cosd(y_deg)
];
end

