clear all;
close all;
load common.mat directory distortionUtilsDir targetDir distortionFileSet
addpath(distortionUtilsDir);
textImg = imread([directory 'original_cutted.png']);
backgroundImg = imread([directory 'background.jpg']);
[w, h, ~] = size(textImg);
[bw, bh, ~] = size(backgroundImg);
%Reduce image size to prevent long execution
targetSize = 1000;
worldSize = targetSize * 1.5;
textImg = imresize(textImg, targetSize / max(w, h));
[w, h, ~] = size(textImg);
backgroundImg = imresize(backgroundImg, [worldSize worldSize]);
transform_world = imref2d([w h], [-w/2 w/2], [-h/2 h/2]);
%Generate matrices or load matrices
matricesFile = 'matrices.mat';
if ~exist([targetDir matricesFile], 'file')
    fprintf('Generate matrices and write to file %s\n', [targetDir matricesFile]);
    %x
    x_max = 0.015;
    x_step = 0.005;
    x_matrices(round(x_max*2/x_step) + 1) = struct; %prelocate array
    for x=-x_max:x_step:x_max
        x_matrices(round((x + x_max)/x_step) + 1).deg = x;
        x_matrices(round((x + x_max)/x_step) + 1).mat = get_xrotation(x);
    end
    y_max = 0.015;
    y_step = 0.005;
    y_matrices(round(y_max * 2 / y_step) + 1) = struct;
    for y=-y_max:y_step:y_max
        y_matrices(round((y + y_max)/y_step) + 1).deg = y;
        y_matrices(round((y + y_max)/y_step) + 1).mat = get_yrotation(y);
    end
    z_max= 15;
    z_step = 5;
    z_matrices(round(z_max * 2 / z_step) + 1) = struct;
    for z=-z_max:z_step:z_max
        z_matrices(round((z + z_max)/z_step) + 1).deg = z;
        z_matrices(round((z + z_max)/z_step) + 1).mat = get_zrotation(z);
    end
    if ~exist(targetDir, 'dir')
        mkdir(targetDir)
    end
    save([targetDir matricesFile], 'x_matrices', 'y_matrices', 'z_matrices');
else
    fprintf('Load matrices from file %s\n', [targetDir matricesFile]);
    load([targetDir matricesFile], 'x_matrices', 'y_matrices', 'z_matrices');
end
%Generate pictures
file_prefix = 'dist_';
ind = 1;
wb = waitbar(0, 'Preparing files...');
%Prepare target
if ~exist(targetDir, 'dir')
    mkdir(targetDir)
end
%Generate set
totalFiles = length(x_matrices)*length(y_matrices)*length(z_matrices);
fprintf('Files will be generated: %d\n', totalFiles);
fileset(totalFiles) = struct;
for xi=1:length(x_matrices)
    xm = x_matrices(xi).mat;
    x_deg = x_matrices(xi).deg;
    for yi=1:length(y_matrices)
        ym = y_matrices(yi).mat;
        y_deg = y_matrices(yi).deg;
        for zi=1:length(z_matrices)
            zm = z_matrices(zi).mat;
            z_deg = z_matrices(zi).deg;
            %Create projection
            tform = projective2d(xm * ym * zm);
            %Save
            transformed = imwarp(textImg, transform_world, tform);
            [th, tw, ~] = size(transformed);
            transformed = imresize(transformed, targetSize / max(th, tw));
            transformed = pasteImgInCenter(backgroundImg, transformed);
            fileName = [targetDir file_prefix int2str(ind) '.png'];
            imwrite(transformed, fileName)
            %Write structure
            fileset(ind).file = fileName;
            fileset(ind).x_deg = x_deg;
            fileset(ind).y_deg = y_deg;
            fileset(ind).z_deg = z_deg;
            %
            ind = ind + 1;
            waitbar(ind / totalFiles, wb);
        end
    end
end
%Save to file
save([targetDir distortionFileSet], 'fileset');
close(wb);
